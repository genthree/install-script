#!/bin/bash

# cd into the directory this script is stored in. (from: http://stackoverflow.com/questions/59895)
SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]
do
	TMP="$(cd -P "$(dirname "${SOURCE}")" && pwd)"
	SOURCE="$(readlink "${SOURCE}")"
	[[ "${SOURCE}" != /* ]] && SOURCE="${TMP}/${SOURCE}"
done

export TMP_DIR="$(realpath "$(dirname "${SOURCE}")")"
unset SOURCE

[[ "${TMP_DIR}" == "/tmp/"?* ]] || { echo "Installation directory must reside in /tmp!" >&2; exit 1; }
cd "${TMP_DIR}" || { echo "Could cd into '${TMP_DIR}'!" >&2; exit 1; }


# include helper functions
source "helper.sh"


# set correct umask
umask 0022


# load config (enable auto variable export before sourcing, disable when finished with 'set -a')
status "Loading config.sh"
set -a
SOURCED_BY_INSTALL_SCRIPT=true source "config.sh"
set +a

if [ "${I_HAVE_CAREFULLY_READ_THE_CONFIG}" != "true" ]
then
	echo "You have to read the config carefully and adjust it to your system" >&2
	echo "otherwise it might cause catastrophic and irreversible damage to your system!" >&2
	echo "" >&2
	echo "'I_HAVE_CAREFULLY_READ_THE_CONFIG' must be set to 'true' to continue" >&2
	exit 1
fi

echo "OK"



# let the user do an etc-update
status "Initial etc-update"
@ etc-update


# create user
status "Creating user"
@ setfacl -R -m d:g::---,d:o::--- /home
@ useradd -N -g users -G audio,input,video,wheel -d "${USER_HOME}" -s /bin/bash "${USER_NAME}"
@ passwd -d "${USER_NAME}"
@ setfacl -R -m d:g::---,d:o::---,g::---,o::--- "${USER_HOME}"
@ setfacl -R -m d:g::---,d:o::---,g::---,o::--- /root

# copy public key as authorized ssh key


status "Enabling services"
@ rc-update add alsasound boot
@ rc-update add sshd default
@ rc-update add ntp-client default
