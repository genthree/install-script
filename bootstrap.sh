#!/bin/bash

GIT_REPOSITORY="https://gitlab.com/genthree/install-script.git"

GIT_REPOSITORY_SNAPSHOT="https://gitlab.com/genthree/install-script/repository/archive.tar.bz2?ref=master"
SNAPSHOT_NAME="install-script.tar.bz2"

TMP_DIR="/tmp"
LOCAL_DIR="${TMP_DIR}/genthree-install-script"


# clean directory
[ -d "${LOCAL_DIR}" ] && rm -rf "${LOCAL_DIR}"
mkdir -p "${TMP_DIR}"
cd "${TMP_DIR}" || { echo "Could not cd into '${TMP_DIR}'!" >&2; exit 1; }

# set correct umask
umask 0022

# download installation files
if which git &>/dev/null
then
	echo "cloning git repository '${GIT_REPOSITORY}'"
	git clone "${GIT_REPOSITORY}" "${LOCAL_DIR}"
else
	echo "git not available, downloading latest repository snapshot from '${GIT_REPOSITORY_SNAPSHOT}'"
	echo ""

	if which curl &>/dev/null; then DOWNLOAD_PROGRAM="curl"; DOWNLOAD_PROGRAM_OUTPUT_FILE_SWITCH="-o"
	elif which wget &>/dev/null; then DOWNLOAD_PROGRAM="wget"; DOWNLOAD_PROGRAM_OUTPUT_FILE_SWITCH="-O"
	else echo "Your system needs either curl or wget to continue" >&2; exit 1
	fi

	"${DOWNLOAD_PROGRAM}" "${GIT_REPOSITORY_SNAPSHOT}" "${DOWNLOAD_PROGRAM_OUTPUT_FILE_SWITCH}" "${SNAPSHOT_NAME}"
	tar xvf "${SNAPSHOT_NAME}" --no-same-owner --no-same-permissions
	mv "install-script-master"* "${LOCAL_DIR}"
fi

echo ""
echo "Successfully downloaded install script to ${LOCAL_DIR}"
echo "Your next steps should include:"
echo " [1m*[m Adjust ${LOCAL_DIR}/config.sh to your system"
echo " [1m*[m Then run ${LOCAL_DIR}/install.sh to start the installation"
