#!/bin/bash

# execution protection
[ "${EXECUTED_IN_CHROOT}" != true ] && { echo "This must not be executed manually!" >&2; exit 1; }



# ${TMP_DIR} is still our temporary directory
echo "Chroot successful"
echo

@- cd "${TMP_DIR}" || { echo "Could not cd into '${TMP_DIR}'!" >&2; exit 1; }
@ source /etc/profile

NPROC="$(($(nproc || echo 2) + 1))"



status "Creating boot folder structure"
@ mkdir -p /boot/efi
@ chmod 700 /boot

status "Mounting other partitions"
@ mount "${DEVICE_ESP}" /boot/efi



status "Updating portage tree"
@ emerge-webrsync
@ emerge --sync --quiet

status "Creating /etc/portage/make.conf"
MAKE_CONF_CPU_FLAGS_X86="$("${TMP_DIR}/contrib/cpuid2cpuflags" | cut -d' ' -f2-)"
MAKE_CONF_MAKEOPTS="-j${NPROC}"

@ cp "${TMP_DIR}/contrib/make.conf" /etc/portage/make.conf
@ sed -i "/MAKEOPTS=/s/.*/MAKEOPTS=\"${MAKE_CONF_MAKEOPTS}\"/" /etc/portage/make.conf
@ sed -i "/CPU_FLAGS_X86=/s/.*/CPU_FLAGS_X86=\"${MAKE_CONF_CPU_FLAGS_X86}\"/" /etc/portage/make.conf



status "Configuring system"

# set hostname
echo Setting hostname
@ sed -i "/hostname=/s/.*/hostname=\"${HOSTNAME}\"/" /etc/conf.d/hostname

# set keymap
echo Setting keymap
@ sed -i "/keymap=/s/.*/keymap=\"${KEYMAP}\"/" /etc/conf.d/keymaps

# set timezone
echo Selecting timezone
@! echo "${TIMEZONE}" '>' /etc/timezone
@ emerge --config sys-libs/timezone-data

# set locale
echo Generating locale
@ cp "${TMP_DIR}/contrib/locale.gen" /etc/locale.gen
@ locale-gen
@ eselect locale set en_US.utf8
@ source /etc/profile

# set root passwd
echo Clearing root password
@ passwd -d root



status "Enabling genthree overlay"

@ mkdir -p /usr/local/overlay /etc/portage/repos.conf
@ cp "${TMP_DIR}/contrib/genthree.conf" /etc/portage/repos.conf/genthree.conf
@ cp -r "${GENTHREE_OVERLAY_REPO_DIR}" /usr/local/overlay/genthree
@ emerge --sync --quiet

status "Selecting profile"
@ eselect profile set "${PORTAGE_PROFILE}"



status "Preparing portage"

# Force upgrade to new portage version which fixes the annoying backtrack bug where slotted dependencies got pulled in unnecessarily
@ emerge --quiet-build=y --update --nodeps --oneshot sys-apps/portage

# If use flags include qt5 we need to solve a circular dependency on harfbuzz
emerge --info | grep "^USE" | grep -q "qt5" && @! 'USE="-qt5"' emerge --quiet-build=y --update --newuse --oneshot cmake

# Install gcc first
@ emerge --quiet-build=y --update --oneshot sys-devel/gcc

# Install layman and git next
@ emerge --quiet-build=y --jobs=${NPROC} --update --oneshot app-portage/layman dev-vcs/git

# Enable overlays via layman
@ rm -rf /usr/local/overlay /etc/portage/repos.conf/genthree.conf

# update layman database
@! 'layman -L 1>/dev/null'

# add genthree repo
@ layman -a genthree
@ emerge --sync --quiet

# set profile again as repo location has changed
@ eselect profile set "${PORTAGE_PROFILE}"



status "Full system update"

# Install Locale-gettext as it is not properly pulled in later for help2man
@ emerge --quiet-build=y --jobs=${NPROC} --update --with-bdeps=y --oneshot dev-perl/Locale-gettext

# update @world
@ emerge --quiet-build=y --jobs=${NPROC} --deep --update --with-bdeps=y --newuse --backtrack=256 @world

# clean dependencies
@ emerge --quiet-build=y --depclean

# rebuild preserved libs
@ emerge --quiet-build=y --jobs=${NPROC} @preserved-rebuild

# run perl-cleaner
@ perl-cleaner --all -- --quiet-build=y

# re-update @world in case something changed
@ emerge --quiet-build=y --jobs=${NPROC} --deep --update --with-bdeps=y --newuse --backtrack=256 @world

# clean dependencies
@ emerge --quiet-build=y --depclean



status "Configuring packages"

# update eix database, set nvim as vi, and vi as default ${EDITOR}
@ eix-update
@ eix-sync
@ eselect vi set nvim
@ eselect editor set /usr/bin/vi

# enable newest gcc version
@ gcc-config "$(gcc-config -l | wc -l)"
@ env-update
@ source /etc/profile



status "Compiling kernel"

# compile kernel
@! emerge --quiet-build=y --jobs=${NPROC} --oneshot sys-kernel/genthree-sources
(
	@ cd /usr/src/linux
	@ make "-j${NPROC}"
	@ make modules_install
	@ make install
)



status "Installing rEFInd"

# rEFInd
@ emerge --quiet-build=y --jobs=${NPROC} --oneshot sys-boot/efibootmgr sys-boot/refind
@ mkdir /boot/efi/EFI
@ refind-install
@ cp "${TMP_DIR}/contrib/refind.conf" /boot/efi/EFI/refind/refind.conf
@! echo "'"'"Linux"  "root='${DEVICE_ROOT}' ro"'"'" '>' /boot/refind_linux.conf



status "Final system configuration"

# enable cron and syslog-ng
echo Enabling cronie and syslog
@ rc-update add cronie default
@ rc-update add syslog-ng default

# fstab
echo Writing fstab
@ cp "${TMP_DIR}/contrib/fstab" /etc/fstab
[ -n "${DEVICE_SWAP}" ] && { @! 'echo "LABEL=${LABEL_SWAP}            none            swap            defaults                                0 0" >> /etc/fstab'; }


echo "Main installation complete"
echo "Execute ${TMP_DIR}/install-secondary.sh for additional system configuration"
