#!/bin/bash

# status prints the arguments given with an underline of ~ as long as the args
status() { local arg="$*"; echo; echo "[1;31m$*[m"; echo "[1;33m$(head -c "${#arg}" < /dev/zero | tr '\0' '~')[m"; }
export -f status

# print_command
print_command() { echo "[1;33m\$[m [1;32m$*[m" >&2; }
export -f print_command

# @- print command, execute command
@-() { print_command "$@"; "$@"; }
export -f @-

# @!- same as @- but eval params instead of calling it (to include bash interpreted construct like pipes '|' in the printed output)
@!-() { print_command "$@"; eval "$@"; }
export -f @!-

# @_helper for @ and @! first param specifies if parameters should be passed to eval
@_helper() {
	local eval_selector="$1"
	shift

	# main loop to be able to retry the command
	while true
	do
		# execute command (with eval if desired)
		if [ "${eval_selector}" == "--eval=y" ]; then @!- "$@"; else @- "$@"; fi
		local exit_code="$?"

		# if unsuccessful prompt user
		if [ ${exit_code} != 0 ]
		then
			# prompt until input is valid
			while true
			do
				echo -n "[1;31mThe last scripted command returned unsuccessful exit status (${exit_code}); Continue installation? [1;31m([1;33mS[1;31mhell/[1;33my[1;31mes/[1;33mn[1;31mo/[1;33mp[1;31mrint/[1;33mr[1;31metry): [m" >&2

				local input=""
				read -r input

				case "${input,,}" in
					"y"|"yes") break ;;
					"n"|"no") exit 1 ;;
					""|"s"|"shell") bash -i ;;
					"p"|"print") print_command "$@" ;;
					"r"|"retry") continue 2 ;; # continue outer while loop
					*) ;;
				esac
			done
		fi

		return ${exit_code}
	done
}
export -f @_helper

# @ print command, execute command, on failure pompt user for action, forward exit status of command
@() { @_helper --eval=n "$@"; }
export -f @

# @! same as @ but eval params like in @!-
@!() { @_helper --eval=y "$@"; }
export -f @!

