# Gentoo Install Script

## Goals

- preconfigure install => hit enter and everything will install
- live distribution independant => bootstrap
- reproducable installation => config
- only for 64 bit systems

## Requirements

- e.g. parted

## Stages

- bootstrap (optional)
	- checkout project to tmp
	- execute script
- livescript
    - check requirements
	- partition scheme
	- create filesystems (check for partition table)
	- mount root partition
	- download newest stage 3 Gentoo
	- extract tar ball in new root
	- network forwarding
	- bind tmp to chroot
	- execute chrootscript 
- chrootscript
	- make.conf => portage conf für host system
	- mount boot partition
	- portage self update = init portage on new system
	- (read news) 
	- set profile = default/(nomultilib)/hardened(for server) or in config
	- timezone
	- locale + locale.gen
	- full system update
	- install packages (config + infos about architecture nvidia/intel/etc)
	- configure and build kernel
	- install and configure refind
- cleanup
	- exit chroot
	- unmount
	- reboot
