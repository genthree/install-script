#!/bin/bash

# execution protection
[ "${SOURCED_BY_INSTALL_SCRIPT}" != true ] && { echo "This must not be executed manually!" >&2; exit 1; }



# auto partitioning
# check necessary devices
status "Checking installation devices"
[ ! -b "${DEVICE_ESP}" ] && { echo "DEVICE_ESP is not a block device!" >&2; exit 1; }
[ ! -b "${DEVICE_ROOT}" ] && { echo "DEVICE_ROOT is not a block device!" >&2; exit 1; }

# check optional devices
[ -n "${DEVICE_SWAP}" ] && [ ! -b "${DEVICE_SWAP}" ] && { echo "DEVICE_SWAP is set but not a block device!" >&2; exit 1; }

echo "OK"



# preparing
status "Unmounting installation devices"
@- umount --lazy "${DEVICE_ESP}"
@- umount --lazy "${DEVICE_ROOT}"



# create filesystems
if [ "${FORMAT_DEVICES}" == true ]
then
	status "Creating file systems"
	@ mkfs.fat -F 32 -n "${LABEL_ESP}" "${DEVICE_ESP}"
	@ mkfs.ext4 -L "${LABEL_ROOT}" "${DEVICE_ROOT}"
	[ -n "${DEVICE_SWAP}" ] && @ mkswap -L "${LABEL_SWAP}" "${DEVICE_SWAP}"
fi



# set partition labels
status "Creating partition labels"

parted_set_name()
{
	local dev_path="$1"
	local name="$2"
	local parent_device
	local partition_number

	# assert device is a block device
	[[ "${dev_path}" == "/dev/"?* ]] || return 1
	local dev="${dev_path:5}"

	# assert device is a partition
	[ -e "/sys/class/block/${dev}/partition" ] || return 1

	# get partition number and parent device path
	partition_number="$(cat "/sys/class/block/${dev}/partition")" || return 1
	parent_device="/dev/$(basename "$(readlink -f "/sys/class/block/${dev}/..")")" || return 1

	@ parted "${parent_device}" --script -- name "${partition_number}" "${name}"
}

parted_set_name "${DEVICE_ESP}" "${LABEL_ESP}" || echo "Could not set partition label for '${DEVICE_ESP}'; Not a partition of a block device." >&2
parted_set_name "${DEVICE_ROOT}" "${LABEL_ROOT}" || echo "Could not set partition label for '${DEVICE_ROOT}'; Not a partition of a block device." >&2
[ -n "${DEVICE_SWAP}" ] && { @ parted_set_name "${DEVICE_SWAP}" "${LABEL_SWAP}" || echo "Could not set partition label for '${DEVICE_SWAP}'; Not a partition of a block device." >&2; }



# download stage3 tarball
status "Downloading newest stage3 tarball"
STAGE3_RELEASES="http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64/"

CURRENT_STAGE3="$("${DOWNLOAD_PROGRAM}" "${STAGE3_RELEASES}" "${DOWNLOAD_PROGRAM_QUIET_SWITCH}" "${DOWNLOAD_PROGRAM_OUTPUT_FILE_SWITCH}" - | grep -o '"stage3-amd64-[0-9A-Z]*.tar.xz"')"
CURRENT_STAGE3="${CURRENT_STAGE3:1:-1}"

@ "${DOWNLOAD_PROGRAM}" "${STAGE3_RELEASES}/${CURRENT_STAGE3}" "${DOWNLOAD_PROGRAM_OUTPUT_FILE_SWITCH}" "${CURRENT_STAGE3}"
@ "${DOWNLOAD_PROGRAM}" "${STAGE3_RELEASES}/${CURRENT_STAGE3}.DIGESTS.asc" "${DOWNLOAD_PROGRAM_OUTPUT_FILE_SWITCH}" "${CURRENT_STAGE3}.DIGESTS.asc"



# verify tarball integrity
status "Verifying tarball integrity"
@ gpg --recv-key "2D182910"
@- gpg --verify "${CURRENT_STAGE3}.DIGESTS.asc" || { echo "gpg signature of '${CURRENT_STAGE3}.DIGESTS.asc' invalid!" >&2; exit 1; }
@! awk "'"'/SHA512 HASH/{getline;print}'"'" '"'${CURRENT_STAGE3}'.DIGESTS.asc" | grep -v ".CONTENTS" | sha512sum --check' || { echo "checksum mismatch!" >&2; exit 1; }



# mount root device
status "Mounting root device"
ROOT_MOUNTPOINT="/gentoo"
@ mkdir -p "${ROOT_MOUNTPOINT}"
@ mount "${DEVICE_ROOT}" "${ROOT_MOUNTPOINT}"



# extract tarball
status "Extracting stage3 tarball"
(
	@ cd "${ROOT_MOUNTPOINT}" && @ tar xpf "${TMP_DIR}/${CURRENT_STAGE3}" --xattrs --numeric-owner
) || { echo "Could not extract tarball to '${ROOT_MOUNTPOINT}'!" >&2; exit 1; }



# prepare genthree overlay
status "Preparing genthree overlay"

GENTHREE_OVERLAY_GIT_REPOSITORY_SNAPSHOT="https://gitlab.com/genthree/overlay/repository/archive.tar.bz2?ref=master"
GENTHREE_OVERLAY_SNAPSHOT_NAME="genthree-overlay.tar.bz2"
export GENTHREE_OVERLAY_REPO_DIR="/tmp/genthree-overlay"

(
	@ cd /tmp
	@ "${DOWNLOAD_PROGRAM}" "${GENTHREE_OVERLAY_GIT_REPOSITORY_SNAPSHOT}" "${DOWNLOAD_PROGRAM_OUTPUT_FILE_SWITCH}" "${GENTHREE_OVERLAY_SNAPSHOT_NAME}"
	@ tar xvf "${GENTHREE_OVERLAY_SNAPSHOT_NAME}" --no-same-owner --no-same-permissions
	@ mv "overlay-master"* "${GENTHREE_OVERLAY_REPO_DIR}"
)



# prepare chroot
status "Preparing chroot environment"
@ cp /etc/resolv.conf "${ROOT_MOUNTPOINT}/etc/resolv.conf"

@ mount -t proc /proc "${ROOT_MOUNTPOINT}/proc"
@ mount --rbind /sys "${ROOT_MOUNTPOINT}/sys"
@ mount --rbind /tmp "${ROOT_MOUNTPOINT}/tmp"
@ mount --make-rslave "${ROOT_MOUNTPOINT}/sys"
@ mount --rbind /dev "${ROOT_MOUNTPOINT}/dev"
@ mount --make-rslave "${ROOT_MOUNTPOINT}/dev"



# begin installation in chroot (${TMP_DIR} is in /tmp and because /tmp is bind mounted into the chroot environment, the path is still valid after chrooting)
status "Chrooting into new system"
EXECUTED_IN_CHROOT=true chroot "${ROOT_MOUNTPOINT}" "${TMP_DIR}/install-chroot.sh"

if [ "$?" == "0" ]; then echo "Installation successful!"; else echo "Installation failed!"; fi
