####################################################################################
# This is the configuration file for the gentoo install script.                    #
#                                                                                  #
# Take the time to read it carefully and adjust it to your system,                 #
# otherwise it might cause catastrophic and irreversible damage to your system!    #
#                                                                                  #
# DO NOT ADD ADDITIONAL VARIABLES OR COMMANDS IN THIS FILE!                        #
####################################################################################

# IGNORE THIS. (execution protection)
[ "${SOURCED_BY_INSTALL_SCRIPT}" != true ] && { echo "This must not be executed manually!" >&2; exit 1; }



##############################
# Configuration begins here. #
##############################

# auto format devices, if set to false make sure that all devices have the correct filesystems created before running the installation
# TODO format per device
# TODO fstype per device
FORMAT_DEVICES=true



# system devices
DEVICE_ESP='/dev/sda1'; LABEL_ESP='efi'
DEVICE_ROOT='/dev/sda2'; LABEL_ROOT='root'

# leave empty to not use a swap partition
DEVICE_SWAP=''; LABEL_SWAP='swap'



# system configuration
TIMEZONE="Europe/Berlin"
KEYMAP="de-latin1-nodeadkeys"
HOSTNAME="localhost"

USER_NAME="myuser"
USER_HOME="/home/${USER_NAME}"

# profile (you might want to select genthree:genthree or genthree:genthree/server for non desktop / server systems)
PORTAGE_PROFILE="genthree:genthree/desktop"



# set to 'true' to allow installation
I_HAVE_CAREFULLY_READ_THE_CONFIG=false
