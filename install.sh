#!/bin/bash

# cd into the directory this script is stored in. (from: http://stackoverflow.com/questions/59895)
SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]
do
	TMP="$(cd -P "$(dirname "${SOURCE}")" && pwd)"
	SOURCE="$(readlink "${SOURCE}")"
	[[ "${SOURCE}" != /* ]] && SOURCE="${TMP}/${SOURCE}"
done

export TMP_DIR="$(realpath "$(dirname "${SOURCE}")")"
unset SOURCE

[[ "${TMP_DIR}" == "/tmp/"?* ]] || { echo "Installation directory must reside in /tmp!" >&2; exit 1; }
cd "${TMP_DIR}" || { echo "Could cd into '${TMP_DIR}'!" >&2; exit 1; }


# include helper functions
source "helper.sh"


# set correct umask
umask 0022



# load config (enable auto variable export before sourcing, disable when finished with 'set -a')
status "Loading config.sh"
set -a
SOURCED_BY_INSTALL_SCRIPT=true source "config.sh"
set +a

if [ "${I_HAVE_CAREFULLY_READ_THE_CONFIG}" != "true" ]
then
	echo "You have to read the config carefully and adjust it to your system" >&2
	echo "otherwise it might cause catastrophic and irreversible damage to your system!" >&2
	echo "" >&2
	echo "'I_HAVE_CAREFULLY_READ_THE_CONFIG' must be set to 'true' to continue" >&2
	exit 1
fi

echo "OK"



# check host system for needed programs
# TODO
status "Preparing installation system"


# prepare a download program
if which curl &>/dev/null; then export DOWNLOAD_PROGRAM='curl'; export DOWNLOAD_PROGRAM_OUTPUT_FILE_SWITCH='-o'; export DOWNLOAD_PROGRAM_QUIET_SWITCH='--silent'
elif which wget &>/dev/null; then export DOWNLOAD_PROGRAM='wget'; export DOWNLOAD_PROGRAM_OUTPUT_FILE_SWITCH='-O'; export DOWNLOAD_PROGRAM_QUIET_SWITCH='--quiet'
else echo "Your system needs either curl or wget to continue" >&2; exit 1
fi



# check network connection by pinging default gateway
@! "ping -q -w 1 -c 1" '"$(ip r | grep default | cut -d " " -f 3)" &>/dev/null' || { echo "Your system must have a internet connection!" >&2; exit 1; }

# set system time
@ ntpd -q -g



# begin installation of base system
SOURCED_BY_INSTALL_SCRIPT=true source "install-live.sh"
